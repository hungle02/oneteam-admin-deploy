"use client";

import { faqfetcher } from "@/interfaces/api";
import React, { Fragment, useEffect, useState } from "react";
import useSWR from "swr";
import LoaderBarWithAddButton from '../../components/Loaders/LoaderBarWithAddButton';
import buttonStyles from '../../styles/modules/buttons.module.scss';
import { IFaq } from "@/interfaces/faq";
import ModalAddFAQ from '../../components/faq/ModalAddFAQ';
import ModalEditFAQ from '../../components/faq/ModalEditFAQ';
import ModalDeleteFAQ from '../../components/faq/ModalDeleteFAQ';
import "bootstrap/dist/css/bootstrap.css";

function FAQ() {
  const { data, error, mutate: refresh } = useSWR("/chat/admin/faq", faqfetcher);
  const [showModalAddFAQ, setShowModalAddFAQ] = useState(false);
  const [showModalEditFAQ, setShowModalEditFAQ] = useState(false);
  const [showModalDeleteFAQ, setShowModalDeleteFAQ] = useState(false);
  const [actionedFAQ, setActionedFAQ] = useState<IFaq | null>(null);
  const [faqs, setFaqs] = useState<IFaq[]>([]);
  const [formSubmitting, setFormSubmitting] = useState(false);

  useEffect(() => {
    if (data) {
      setFaqs(data);
    }
  }, [data]);

  return (
    <>
    <div id="cardHeader">
      <span style={{ fontSize: '20px', fontWeight: 'semi-bold' }}>FAQ</span>
    </div>
    <div id="card" className="pt-0">
      <LoaderBarWithAddButton onClick={refresh} onAdd={() => { setShowModalAddFAQ(true); }} buttonText="Add FAQ" buttonIcon="playlist_add" />
      <hr />
      <br />
      <div className="accordion" id="main-accordian">
        {
          faqs?.map((faq, ind) => (
            <Fragment key={faq.id}>
              <div className={'d-flex w-100 mb-3'}>
                <div className="flex-grow-1">
                  <div key={faq.id} className="card text-dark bg-light">
                    <div className="card-header">{faq.question}</div>
                    <div className="card-body">
                      <p className="card-text">{faq.answer}</p>
                    </div>
                  </div>
                </div>
                <div className="d-flex flex-column gap-3" style={{ width: '50px', paddingLeft: '10px' }}>
                  <button
                    type="button"
                    className={`${buttonStyles.editButton}`}
                    style={{ height: '40px', width: '40px', textAlign: 'center' }}
                    disabled={formSubmitting}
                    onClick={() => { setActionedFAQ(faq); setShowModalEditFAQ(true); }}
                  >
                    <span className="material-icons" style={{ lineHeight: '40px', fontSize: '16px', width: '100%' }}>edit</span>
                  </button>
                  <button
                    type="button"
                    className={`${buttonStyles.deleteButton}`}
                    style={{ height: '40px', width: '40px', textAlign: 'center' }}
                    disabled={formSubmitting}
                    onClick={() => { setActionedFAQ(faq); setShowModalDeleteFAQ(true); }}
                  >
                    <span className="material-icons" style={{ lineHeight: '40px', fontSize: '16px', width: '100%' }}>delete</span>
                  </button>
                </div>
              </div>
            </Fragment>
          ))
        }
      </div>
    </div>
    {showModalAddFAQ && (
      <ModalAddFAQ setShowModal={setShowModalAddFAQ} callback={refresh} />
    )}
    {showModalEditFAQ && (
      <ModalEditFAQ faq={actionedFAQ} setShowModal={setShowModalEditFAQ} callback={refresh} />
    )}
    {showModalDeleteFAQ && (
      <ModalDeleteFAQ faq={actionedFAQ} setShowModal={setShowModalDeleteFAQ} callback={refresh} />
    )}
  </>
  )
}

export default FAQ;