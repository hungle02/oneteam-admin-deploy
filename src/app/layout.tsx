import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import "../styles/toast.css";
import "../styles/sidebar.css";
// import "bootstrap/dist/css/bootstrap.css";
import { UserProvider } from "../contexts/UserContext";
import { Providers } from "@/Store/Provider";
import { ToastProvider } from "../contexts/ToastContext";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "One Team Fashion",
  description: "Admin dashboard",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <link
          href="https://fonts.googleapis.com/css2?family=Material+Icons&display=block"
          rel="stylesheet"
        />
      </head>
      <body className={inter.className}>
        <ToastProvider>
          <Providers>
            <UserProvider>{children}</UserProvider>
          </Providers>
        </ToastProvider>
      </body>
    </html>
  );
}
