"use client";
import React, { useState } from "react";
import { GoogleLogin } from "@react-oauth/google";
import Image from "next/image";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { Config } from '@/config';

const Admin = () => {
  const [errorMessage, setErrorMessage] = useState<string>("");
  const handleGoogleLogin = async (token: string | undefined) => {
    const response = await fetch(`${Config.BACKEND_URL}auth/sign-in`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ idToken: token }),
    });

    const data = await response.json();
    if (response.ok) {
      console.log("Signin successful", data);
      localStorage.setItem("accessToken", data.token);
      if (data.user.role === "admin") {
        window.location.href = "/admin";
      }
    } else {
      setErrorMessage(data.message);
    }
  };
  return <div className="text-black bg-slate-700 ">Hahahaha</div>;
};

export default Admin;
