"use client";
import React, { useState } from "react";
import { GoogleLogin } from "@react-oauth/google";
import Image from "next/image";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { Config } from '@/config';

const Login = () => {
  const [errorMessage, setErrorMessage] = useState<string>("");
  const handleGoogleLogin = async (token: string | undefined) => {
    const response = await fetch(`${Config.BACKEND_URL}auth/admin/sign-in`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ idToken: token }),
    });

    const data = await response.json();
    if (response.ok) {
      console.log("Signin successful", data);
      if (data.user.role === "admin") {
        localStorage.setItem("accessToken", data.token);
        window.location.href = "/dashboard";
      }
    } else {
      setErrorMessage(data.message);
    }
  };
  return (
    <GoogleOAuthProvider clientId="430094969846-np6juvnqerofvlq9pfn1s3d5bl2eeo8g.apps.googleusercontent.com">
      <div className="text-black bg-white flex flex-col" style={{ height: "100vh" }}>
        <Image
          src={"/thumb/oneteam.png"}
          quality={100}
          alt="Oneteam"
          width={0}
          height={0}
          sizes="100vw"
          style={{ width: "100%", height: "auto" }} // optional
        />
        <div className="flex flex-col items-center flex-1 justify-center">
          <p className="text-3xl font-bold text-center mb-6">Sign in or Sign up as Admin</p>
          <div className="px-5 py-3 bg-white rounded-3xl">
            <GoogleLogin
              onSuccess={(credentialResponse) => {
                handleGoogleLogin(credentialResponse.credential);
              }}
              onError={() => {
                console.log("Login Failed");
              }}
            />

            {errorMessage && (
              <p className=" text-center pt-5 text-base text-red-400">{errorMessage}</p>
            )}
          </div>
        </div>
      </div>
    </GoogleOAuthProvider>
  );
};

export default Login;
