"use client";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../Store/store";
import { IOrderItem, OrderItems, OrderStatus } from "@/components/OrdersDetailsDataTable";

export default function GettingDatasLength() {
  const userData = useSelector((state: RootState) => state.Admin.userNumber);

  const catData = useSelector((state: RootState) => state.Admin.category);
  const prodData = useSelector((state: RootState) => state.Admin.product);
  const orderData = useSelector((state: RootState) => state.Admin.Order);

  return [
    {
      icon: "FaUserAlt",
      color: "text-green-600",
      title: "Total Users",
      count: userData || 0,
    },
    {
      icon: "GiAbstract010",
      color: "text-blue-600",
      title: "Total Products",
      count: prodData?.length || 0,
    },
    {
      icon: "CgMenuGridR",
      color: "text-purple-600",
      title: "Total Categories",
      count: catData?.length || 0,
    },
    {
      icon: "AiOutlineClockCircle",
      color: "text-yellow-600",
      title: "Pending Orders",
      count: orderData?.filter((order) => order.status === "pending").length,
    },
    {
      icon: "GrCompliance",
      color: "text-orange-600",
      title: "Completed Orders",
      count: orderData?.filter((order) => order.status === "delivered").length,
    },
  ];
}
