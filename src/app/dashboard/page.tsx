"use client";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import AdminNavbar from "@/components/AdminNavbar";
import AdminSidebar from "@/components/AdminSidebar";
import SuperComponent from "@/components/SuperComponent";
import { ToastContainer, toast } from "react-toastify";
import useSWR from "swr";
import { get_all_categories } from "@/Services/Admin/category";
import { useDispatch } from "react-redux";
import {
  setUserNumber,
  setCatLoading,
  setCategoryData,
  setOrderData,
  setProdLoading,
  setProductData,
} from "@/utils/AdminSlice";
import Loading from "../loading";
import { setNavActive } from "@/utils/AdminNavSlice";
import {
  get_all_products,
  get_order_number,
  get_product_number,
  get_user_number,
} from "@/Services/Admin/product";
import { get_all_orders } from "@/Services/Admin/order";
import { set } from "react-hook-form";

export interface userData {
  email: String;
  role: String;
  _id: String;
  name: String;
}

export default function Dashboard() {
  const Router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    // const user: userData | null = localStorage.getItem("accessToken");
    const accessToken = localStorage.getItem("accessToken");
    if (!accessToken) {
      Router.push("/");
    }
    dispatch(setNavActive("Base"));
  }, [dispatch, Router]);

  const { data: categoryData, isLoading: categoryLoading } = useSWR(
    "/gettingAllCategoriesFOrAdmin",
    get_all_categories
  );
  if (categoryData?.success !== true) toast.error(categoryData?.message);

  const { data: productData, isLoading: productLoading } = useSWR(
    "/gettingAllProductsFOrAdmin",
    get_all_products
  );

  if (productData?.success !== true) toast.error(productData?.message);

  const { data: orderData, isLoading: orderLoading } = useSWR(
    "/gettingAllOrdersForAdmin",
    get_all_orders
  );
  if (orderData?.success !== true) toast.error(orderData?.message);
  const { data: userData, isLoading: userLoading } = useSWR(
    "/gettingUserNumberForAdmin",
    get_user_number
  );

  console.log("categoryDate", categoryData);
  console.log("orderData", orderData);
  console.log("productDate", productData?.products);

  useEffect(() => {
    dispatch(setUserNumber(userData?.user));
    dispatch(setCategoryData(categoryData));
    dispatch(setCatLoading(categoryLoading));
    dispatch(setProductData(productData?.products));
    dispatch(setProdLoading(productLoading));
    dispatch(setOrderData(orderData));
    dispatch(setCatLoading(orderLoading));
  }, [
    userData,
    categoryData,
    dispatch,
    categoryLoading,
    productData,
    productLoading,
    orderData,
    orderLoading,
  ]);

  return (
    <div className="w-full h-screen flex  bg-gray-50 overflow-hidden">
      <AdminSidebar />
      <div className="w-full h-full ">
        <AdminNavbar />
        <div className="w-full  flex flex-wrap items-start justify-center overflow-y-auto py-2">
          {categoryLoading || productLoading ? <Loading /> : <SuperComponent />}
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}
