import { Flex, Spinner } from "@chakra-ui/react";

export default function Loading() {
  // You can add any UI inside Loading, including a Skeleton.
  return (
    <Flex width="80vw" height="80vh" alignItems="center" justifyContent="center">
      <Spinner boxSize={100} />
    </Flex>
  );
}
