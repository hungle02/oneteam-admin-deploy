"use client";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { Flex } from "@chakra-ui/react";
import { Spinner } from "@chakra-ui/react";

export default function Home() {
  const { push } = useRouter();
  useEffect(() => {
    push("/authen/login");
  }, []);
  return (
    <>
      <main className="flex min-h-screen flex-col items-center justify-between p-24 ">
        <Flex width="80vw" height="80vh" alignItems="center" justifyContent="center">
          <Spinner boxSize={100} />
        </Flex>
      </main>
    </>
  );
}
