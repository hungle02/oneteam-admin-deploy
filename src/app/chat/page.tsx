"use client";
import {
  ChakraProvider,
  Flex,
  Text,
  VStack,
  StackDivider,
  Button,
  FormControl,
  HStack,
  IconButton,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spacer,
  useDisclosure,
  Switch,
} from "@chakra-ui/react";
import React, { ChangeEvent, useEffect, useState } from "react";
import Divider from "../../components/divider";
import Footer from "../../components/footer";
import ChatHeader from "../../components/chatheader";
import Messages from "../../components/messages";
import { FaEdit, FaTrash, FaPlus } from "react-icons/fa";
import { FiSettings } from "react-icons/fi";
import { HiChatAlt2 } from "react-icons/hi";
import { RiKakaoTalkFill } from "react-icons/ri";
import type { Message, Chat } from "../../interfaces";
import { Spinner } from "@chakra-ui/react";
import {
  chatAllfetcher,
  chatGetMsgs,
  chatAddMsg,
  chatCreate,
  chatDeleteById,
  chatUpdateById,
  chatGetResponse,
} from "../../interfaces/api";
import useSWR from "swr";
import { v4 as uuidv4 } from "uuid";
import { id } from "date-fns/locale";
import notificationWS from "../../utils/notificationWS";
import { ICustomer } from "../../interfaces/chat";

const Chat = () => {
  const { data, error } = useSWR("/chat/admin/list-start-message", chatAllfetcher);
  // add an alias to data and error
  const [targetChatRoomId, setTargetChatRoomId] = useState("");
  const [messages, setMessages] = useState<Message[]>([]);
  const [chatrooms, setChatrooms] = useState<Chat[]>([]);
  const [currentCustomer, setCurrentCustomer] = useState<ICustomer | null>(null);
  // const uniqueId = () => Math.round(Date.now() * Math.random());
  const { connectWS, updateMessage, broadcast } = notificationWS();

  const [targetChatRoomName, setTargetChatRoomName] = useState("");
  const [initFlag, setInitFlag] = useState(false);
  const [inputMessage, setInputMessage] = useState("");
  const [isInThinking, setIsInThinking] = useState(false);
  const [isGPTmode, setIsGPTmode] = useState(false);

  const { isOpen, onOpen, onClose } = useDisclosure();

  useEffect(() => {
    connectWS();
  }, []);

  useEffect(() => {
    if (initFlag) {
      setMessages([]);
      setInitFlag(false);
    }
  }, [initFlag]);

  useEffect(() => {
    // error handling, if data is null, return empty array
    if (data) {
      if (data.length === 0) {
        setChatrooms([]);
      } else {
        const chatRooms: Chat[] = data.map((chat: Chat) => ({
          id: chat.channelId,
          name: chat.nickName || chat.username,
          messages: [chat.content],
          photo: chat.photo,
        }));
        setChatrooms(chatRooms);
        handleChatSelect(targetChatRoomId);
      }
    }
  }, [data]);

  if (error) return <div>failed to load</div>;
  if (!data)
    return (
      <div className="flex min-h-screen flex-col items-center justify-between p-24 ">
        <Flex width="80vw" height="80vh" alignItems="center" justifyContent="center">
          <Spinner boxSize={100} />
        </Flex>
      </div>
    );

  const addMessage = (message: Message) => {
    setMessages((old) => [...old, message]);
    setInputMessage("");
    handleUpdateMessagesInChatRooms(targetChatRoomId, message);
  };

  const handleSendMessage = (targetChatRoomId: string) => {
    const message = {
      channelId: targetChatRoomId,
      message: {
        _id: 1,
        text: inputMessage,
        createdAt: new Date(),
      },
      isAdmin: true,
    };
    broadcast(JSON.stringify(message));
    const newMessage = {
      chat_id: targetChatRoomId,
      id: uuidv4(),
      from_who: "me",
      msg: inputMessage,
    };
    addMessage(newMessage);
    // if (!inputMessage.trim().length) {
    //   return;
    // }
    // setIsInThinking(true);
    // const newMessageMeId = uuidv4();
    // const newMessage = {
    //   chat_id: targetChatRoomId,
    //   id: newMessageMeId,
    //   from_who: "me",
    //   msg: inputMessage,
    // };

    // setMessages((old) => [...old, newMessage]);
    // setInputMessage("");
    // handleUpdateMessagesInChatRooms(targetChatRoomId, newMessage);

    // const newMessageComId = uuidv4();
    // setTimeout(() => {
    //   const botResponsePromise = chatGetResponse(
    //     targetChatRoomId,
    //     inputMessage,
    //     isGPTmode ? "gpt" : "work"
    //   );
    //   // get value from const botResponse: Promise<any>
    //   botResponsePromise
    //     .then((botResponse) => {
    //       console.log(botResponse);
    //       const newBotMessage = {
    //         chat_id: targetChatRoomId,
    //         id: newMessageComId,
    //         from_who: "computer",
    //         msg: botResponse,
    //       };
    //       // Check if botResponse has status property and stauts is not 200
    //       if (botResponse.hasOwnProperty("status") && botResponse.status !== 200) {
    //         alert("Error: " + botResponse.statusText);
    //         setIsInThinking(false);
    //         return;
    //       }
    //       setMessages((old) => [...old, newBotMessage]);
    //       handleUpdateMessagesInChatRooms(targetChatRoomId, newBotMessage);
    //       if (newBotMessage) {
    //         setIsInThinking(false);
    //       }
    //     })
    //     .catch((error) => {
    //       alert(error);
    //       setIsInThinking(false);
    //     });
    // }, 300);
  };

  const handleUpdateMessagesInChatRooms = (targetChatRoomId: string, newMessage: Message) => {
    setChatrooms((oldChatrooms) => {
      const updatedChatrooms = [...oldChatrooms];
      const index = updatedChatrooms.findIndex((chatroom) => chatroom.id === targetChatRoomId);

      if (index != -1) {
        updatedChatrooms[index] = {
          ...updatedChatrooms[index],
          messages: [...(updatedChatrooms[index].messages ?? []), newMessage],
        };
      }

      return updatedChatrooms;
    });

    chatAddMsg(targetChatRoomId, newMessage);
  };

  updateMessage(targetChatRoomId, addMessage);

  const handleAdd = () => {
    const timestamp = new Date().toLocaleString("ja-JP");
    const newChatRoomId = uuidv4();
    const newChatRoom = { id: newChatRoomId, name: `${timestamp}` };
    setChatrooms((prevChatrooms) => [newChatRoom, ...prevChatrooms]);

    setTargetChatRoomId(newChatRoomId);
    chatCreate(newChatRoom);

    const newMessage = {
      chat_id: newChatRoomId,
      id: uuidv4(),
      from_who: "computer",
      msg: `Hi! Chat ${timestamp}`,
    };
    setMessages([newMessage]);
    handleUpdateMessagesInChatRooms(newChatRoomId, newMessage);
    setIsInThinking(false);
  };

  const handleEdit = (targetChatRoomId: string) => {
    onOpen();
    setTargetChatRoomId(targetChatRoomId);
    const chatRoom = chatrooms.find((chatroom) => chatroom.id === targetChatRoomId);
    setTargetChatRoomName(chatRoom ? chatRoom.name : "");

    if (chatRoom) {
      chatUpdateById(targetChatRoomId, chatRoom ? chatRoom.name : "");
    }
  };

  const handleSave = () => {
    setChatrooms((prevChatrooms) =>
      prevChatrooms.map((chatroom) =>
        chatroom.id === targetChatRoomId ? { ...chatroom, name: targetChatRoomName } : chatroom
      )
    );
    if (targetChatRoomName) {
      // const updateChat = { id: targetChatRoomId, name: targetChatRoomName };
      chatUpdateById(targetChatRoomId, targetChatRoomName);
    }
    onClose();

    // setTargetChatRoomName("");
  };

  const handleChatSelect = async (targetChatRoomId: string) => {
    if (targetChatRoomId !== null && targetChatRoomId !== undefined && targetChatRoomId !== "") {
      setTargetChatRoomId(targetChatRoomId);
      const reponse = await chatGetMsgs(targetChatRoomId);
      if (reponse) {
        const messages: Message[] = reponse.map((msg: Message) => ({
          chat_id: msg.id,
          id: msg.id,
          created_at: msg.createdAt,
          msg: msg.content,
          from_who: msg.isAdmin ? "me" : "computer",
        }));
        const sortedChatRoomMsgs = messages.sort((a: Message, b: Message) => {
          if (a.created_at && b.created_at) {
            const dateA = new Date(a.created_at);
            const dateB = new Date(b.created_at);
            return dateA.getTime() - dateB.getTime();
          } else {
            return 0;
          }
        });
        setMessages(sortedChatRoomMsgs ? sortedChatRoomMsgs : []);

        // current user
        const currentCustomer = chatrooms.filter((chatroom) => chatroom.id === targetChatRoomId)[0];
        setCurrentCustomer({ userName: currentCustomer.name, photo: currentCustomer.photo });
      }
    }
  };

  function handleMode(event: ChangeEvent<HTMLInputElement>): void {
    setIsGPTmode(!isGPTmode);
  }

  return (
    <ChakraProvider>
      <Flex w="100%" h="92vh" justify="center" align="center">
        <Flex w="18%" h="92vh" bg="gray.200">
          <VStack width="100%">
            <VStack
              divider={<StackDivider />}
              p={4}
              width="100%"
              height="92%"
              maxW={{ base: "90vw", sm: "80vw", lg: "50vw", xl: "40vw" }}
              alignItems="stretch"
              bg="gray.100"
            >
              {/* Chat Add Panel */}
              <HStack>
                <Text fontSize="2xl" fontWeight="bold">
                  Chats
                </Text>
                <Spacer />
              </HStack>
              {/* Chat Select Panel */}
              {/* Uncaught TypeError: chatrooms.map is not a function */}
              {chatrooms?.map((rooms) => (
                <HStack
                  key={`h${rooms.id}`}
                  onClick={() => handleChatSelect(rooms.id)}
                  style={{
                    cursor: "pointer",
                    fontWeight: rooms.id === targetChatRoomId ? "bold" : "normal",
                  }}
                >
                  {/* <IconButton
                    icon={<HiChatAlt2 />}
                    isRound={true}
                    key={`iba${rooms.id}`}
                    aria-label={""}
                  /> */}
                  <img
                    alt="User image"
                    src={
                      rooms.photo ||
                      "https://www.iconpacks.net/icons/2/free-user-icon-3296-thumb.png"
                    }
                    style={{ width: "3rem", height: "3rem", borderRadius: "50%" }}
                  />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                      marginLeft: "0.3rem",
                    }}
                  >
                    <span>{rooms.name}</span>
                  </div>
                  <Spacer />
                  <IconButton
                    key={`ibb${rooms.id}`}
                    icon={<FaEdit />}
                    isRound={true}
                    onClick={() => handleEdit(rooms.id)}
                    aria-label={""}
                  />
                  <Modal key={`m${rooms.id}`} isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                      <ModalHeader>Edit Nickname</ModalHeader>
                      <ModalCloseButton />
                      <ModalBody pb={6}>
                        <FormControl>
                          <Input
                            key={`mi${rooms.id}`}
                            placeholder="Edit"
                            value={targetChatRoomName}
                            onChange={(e) => setTargetChatRoomName(e.target.value)}
                          />
                        </FormControl>
                      </ModalBody>
                      <ModalFooter>
                        <Button colorScheme="blue" mr={3} onClick={() => handleSave()}>
                          Save
                        </Button>
                        <Button onClick={onClose}>Cancel</Button>
                      </ModalFooter>
                    </ModalContent>
                  </Modal>
                  {/* <IconButton
                    key={`ibc${rooms.id}`}
                    icon={<FaTrash />}
                    isRound={true}
                    aria-label={""}
                    onClick={() => handleDelete(rooms.id)}
                  /> */}
                </HStack>
              ))}
            </VStack>
          </VStack>
        </Flex>
        <Flex w="5px" h="92vh"></Flex>
        {/* Chat Panel */}
        <Flex w="82%" h="92vh">
          <Flex w="98%" h="100%" flexDir="column" mt={2}>
            <ChatHeader />
            <Divider />
            <Messages
              messages={messages}
              photo={
                currentCustomer?.photo ||
                "https://www.iconpacks.net/icons/2/free-user-icon-3296-thumb.png"
              }
            />
            {/* <Divider /> */}
            <Footer
              inputMessage={inputMessage}
              setInputMessage={setInputMessage}
              handleSendMessage={() => handleSendMessage(targetChatRoomId)}
            />
          </Flex>
        </Flex>
      </Flex>
    </ChakraProvider>
  );
};

export default Chat;
