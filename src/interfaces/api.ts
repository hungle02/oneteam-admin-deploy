import axios from "axios";
import { Chat, Message } from ".";
import { Config } from '@/config';

export const api = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL || Config.BACKEND_URL,
  withCredentials: false,
});

export const chatAllfetcher = (url: string) =>
  api
    .get(url)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const faqfetcher = (url: string) =>
  api
    .get(url)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatCreate = (newChat: Chat) =>
  api
    .post("/api/chat", newChat)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatGetById = (chat_id: string) =>
  api
    .get(`/api/chat/${chat_id}`)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatGetMsgs = (chat_id: string) =>
  api
    .get(`/chat/admin/messages/${chat_id}`)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatUpdateById = (chat_id: string, nickName: string) =>
  api
    .put(`/auth/users/${chat_id}`, {nickName: nickName})
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatDeleteById = (chat_id: string) =>
  api
    .delete(`/api/chat/${chat_id}`)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatAddMsg = (chat_id: string, addmsg: Message) =>
  api
    .post(`/api/chat/${chat_id}/message`, addmsg)
    .then((res) => res.data)
    .catch((error) => console.log(error));

export const chatGetResponse = (chat_id: string, msg: string, mode: string) =>
  api
    .post(`/api/chat/response`, { chat_id: chat_id, msg: msg, mode: mode })
    .then((res) => res.data)
    .catch((error) => console.log(error));

// export const chatAllfetcher = (url: string) => {
//   const mockMessages: Message[] = [
//     {
//       chat_id: "1",
//       id: "1",
//       msg: "Hello there!",
//       from_who: "user",
//       created_at: new Date(1716953123608),
//       updated_at: new Date(1716953123608),
//     },
//   ];
//   // Mockup response JSON
//   const mockResponse: Chat[] = [
//     {
//       id: "1",
//       name: "Chat 1",
//       prompt: "Hello there!",
//       messages: mockMessages,
//     },
//   ];

//   return Promise.resolve(mockResponse);
// };
