import { useState } from 'react';
import styles from './LoaderBar.module.css';
import buttonStyles from '../../styles/modules/buttons.module.scss';

export default function LoaderBar({ onClick, mt = true } : any) {
  const [showLoader, setShowLoader] = useState(false);

  async function refresh() {
    setShowLoader(true);
    await onClick();
    setShowLoader(false);
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className={`col-12 p-0 ${mt ? 'mt-3 ' : ''}mb-3 position-relative`}>
          {showLoader && (
          <div className={styles.loaderContainer}>
            <div className={styles.loader}>Loading</div>
          </div>
          )}
          <div>
            <button type="button" className={buttonStyles.refreshButton} onClick={refresh}>
              <span className="material-icons">refresh</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
