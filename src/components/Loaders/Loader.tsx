import styles from './LoaderBar.module.css';

export default function Loader({ tx = true }) {
  const style = {transform: ''};
  if (tx) {
    style.transform = 'translateY(-28px)';
  }
  return (
    <div className={styles.loaderContainer} style={style}>
      <div className={styles.loader}>Loading</div>
    </div>
  );
}
