"use client";

import { useState } from 'react';
import styles from './LoaderBar.module.css';
import buttonStyles from '../../styles/modules/buttons.module.scss';

export default function LoaderBarWithAddButton({
  onClick, onAdd, buttonText, buttonIcon, mt = true, disabled = false
}) {
  const [showLoader, setShowLoader] = useState(false);

  async function refresh() {
    setShowLoader(true);
    await onClick();
    setShowLoader(false);
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className={`col-12 p-0 ${mt ? 'mt-3 ' : ''}mb-3 position-relative`}>
          {showLoader && (
            <div className={styles.loaderContainer}>
              <div className={styles.loader}>Loading</div>
            </div>
          )}
          <div>
            <button type="button" className={buttonStyles.refreshButton} onClick={refresh}>
              <span className="material-icons">refresh</span>
            </button>
          </div>
          <button type="button" disabled={disabled} className={buttonStyles.addButton} onClick={onAdd}>
            <span className="material-icons" style={{ lineHeight: '28px', fontSize: '16px', paddingRight: '5px' }}>{buttonIcon}</span>
            <span>{buttonText}</span>
          </button>
        </div>
      </div>
    </div>
  );
}
