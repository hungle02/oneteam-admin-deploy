"use Client";

import React, { useEffect, useState } from "react";

import { useSWRConfig } from "swr";
import DataTable from "react-data-table-component";
import { useSelector } from "react-redux";
import { RootState } from "@/Store/store";
import { useRouter } from "next/navigation";

export interface OrderItems {
  id: string;
  size: string;
  sizeId: string;
  image: string;
  price: number;
  discountRate: number;
}

export interface IOrderItem {
  quantity: number;
  productName: string;
  productItem: OrderItems;
}

export enum OrderStatus {
  pending = "pending",
  delivered = "delivered",
}

export interface Order {
  id: string;
  createdAt: string;
  status: string;
  userId: string;
  createAt: string;
  orderItems: IOrderItem[];
}

// interface Order {
//   createdAt: string;
//   deliveredAt: string;
//   isDelivered: boolean;
//   isPaid: boolean;
//   itemsPrice: number;
//   orderItems: {
//     qty: number;
//     product: {
//       createdAt: string;
//       productCategory: string;
//       productDescription: string;
//       productFeatured: boolean;
//       productImage: string;
//       productName: string;
//       productPrice: number;
//       productQuantity: number;
//       productSlug: string;
//       updatedAt: string;
//       __v: number;
//       _id: string;
//     };
//     _id: string;
//   }[];
//   paidAt: string;
//   paymentMethod: string;
//   shippingAddress: {
//     address: string;
//     city: string;
//     country: string;
//     fullName: string;
//     postalCode: number;
//   };
//   shippingPrice: number;
//   taxPrice: number;
//   totalPrice: number;
//   updatedAt: string;
//   user: {
//     email: string;
//     name: string;
//     password: string;
//     role: string;
//     __v: number;
//     _id: string;
//   };
//   __v: number;
//   _id: string;
// }

export default function OrdersDetailsDataTable() {
  const { mutate } = useSWRConfig();
  const router = useRouter();
  const [orderData, setOrderData] = useState<Order[] | []>([]);
  const data = useSelector((state: RootState) => state.Order.order) as Order[] | [];
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState<Order[] | []>([]);

  useEffect(() => {
    setOrderData(data);
  }, [data]);

  useEffect(() => {
    setFilteredData(orderData);
  }, [orderData]);

  const columns = [
    {
      name: "Order ID",
      selector: (row: Order) => row?.id,
      sortable: true,
    },
    {
      name: "Total Price",
      selector: (row: Order) =>
        row?.orderItems.reduce(
          (acc, currItem: IOrderItem) =>
            acc +
            ((currItem.productItem.price * (100 - currItem.productItem.discountRate || 0)) / 100) *
              currItem.quantity,
          0
        ),
      sortable: true,
    },
    {
      name: "Delivered",
      selector: (row: Order) => (row?.status == OrderStatus.delivered ? "Yes" : "No"),
      sortable: true,
    },
    {
      name: "Action",
      cell: (row: Order) => (
        <button
          onClick={() => router.push(`/order/view-orders-details/${row?.id}`)}
          className=" w-20 py-2 mx-2 text-xs text-green-600 hover:text-white my-2 hover:bg-green-600 border border-green-600 rounded transition-all duration-700"
        >
          Details
        </button>
      ),
    },
  ];

  useEffect(() => {
    if (search === "") {
      setFilteredData(orderData);
    } else {
      setFilteredData(
        orderData?.filter((item) => {
          const itemData = item?.id?.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        })
      );
    }
  }, [search, orderData]);

  return (
    <div className="w-full h-full">
      <DataTable
        columns={columns}
        data={filteredData || []}
        key={"ThisOrdersData"}
        pagination
        keyField="id"
        title={`Orders list`}
        fixedHeader
        fixedHeaderScrollHeight="700px"
        selectableRows
        selectableRowsHighlight
        persistTableHead
        subHeader
        subHeaderComponent={
          <input
            className="w-60 dark:bg-transparent py-2 px-2  outline-none  border-b-2 border-orange-600"
            type={"search"}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={"Orders ID"}
          />
        }
        className="bg-white px-4 h-5/6 "
      />
    </div>
  );
}
