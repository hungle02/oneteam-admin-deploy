"use client";

import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
// import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';
import { ToastContainer, toast } from "react-toastify";
import { TailSpin } from "react-loader-spinner";
import { useRouter } from "next/navigation";
import { add_new_category, get_parent_category } from "@/Services/Admin/category";
import Cookies from "js-cookie";
import useSWR from "swr";
import { CategoryData } from "../CategoryDataTable";

type Inputs = {
  name: string;
  parentID?: string;
};

interface loaderType {
  loader: Boolean;
}

const uploadImages = async (file: File) => {
  // const createFileName = () => {
  //   const timestamp = Date.now();
  //   const randomString = Math.random().toString(36).substring(2, 8);
  //   return `${file?.name}-${timestamp}-${randomString}`;
  // };

  // const fileName = createFileName();
  // const storageRef = ref(storage, `ecommerce/category/${fileName}`);
  // const uploadTask = uploadBytesResumable(storageRef, file);

  return new Promise((resolve, reject) => {
    // uploadTask.on('state_changed', (snapshot) => {
    // }, (error) => {
    //     console.log(error)
    //     reject(error);
    // }, () => {
    //     getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
    //         resolve(downloadURL);
    //     }).catch((error) => {
    //         console.log(error)
    //         reject(error);
    //     });
    // });
  });
};

// const maxSize = (value: File) => {
//   const fileSize = value.size / 1024 / 1024;
//   return fileSize < 1 ? false : true;
// };

// interface userData {
//   email: String;
//   role: String;
//   _id: String;
//   name: String;
// }

export default function AddCategory() {
  const [loader, setLoader] = useState(false);
  const Router = useRouter();

  const { data: categoryData, isLoading: categoryLoading } = useSWR(
    "/getParentCategories",
    get_parent_category
  );

  useEffect(() => {
    const accessToken = localStorage.getItem("accessToken");
    if (!accessToken) {
      Router.push("/");
    }
  }, [Router]);

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<Inputs>({
    criteriaMode: "all",
  });

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    setLoader(true);
    // const CheckFileSize = maxSize(data.image[0]);
    // if (CheckFileSize) return toast.error("Image size must be less then 1MB");
    // const uploadImageToFirebase = await uploadImages(data.image[0]);
    // const uploadImageToFirebase = 'https://firebasestorage.googleapis.com/v0/b/socialapp-9b83f.appspot.com/o/ecommerce%2Fcategory%2Fimages131.jpg-1683339363348-c4vcab?alt=media&token=f9303ff9-7d34-4514-a53f-832f72814337';

    const finalData = {
      title: data.name,
      parentId: data.parentID,
    };
    if (data.parentID == "-1") {
      finalData.parentId = undefined;
    }
    const res = await add_new_category(finalData);
    if (res.success) {
      toast.success(res?.message);
      setTimeout(() => {
        Router.push("/Dashboard");
      }, 2000);
      setLoader(false);
    } else {
      toast.error(res?.message);
      setLoader(false);
    }
  };

  return (
    <div className="w-full h-full bg-white flex flex-col ">
      <div className="w-full my-2 text-left">
        <h1 className="text-xl px-4 py-2">Add Category</h1>
      </div>
      {loader || categoryLoading ? (
        <div className="w-full  flex-col h-96 flex items-center justify-center ">
          <TailSpin
            height="50"
            width="50"
            color="orange"
            ariaLabel="tail-spin-loading"
            radius="1"
            wrapperStyle={{}}
            wrapperClass=""
            visible={true}
          />
          <p className="text-sm mt-2 font-semibold text-orange-500">
            Adding Category Hold Tight ....
          </p>
        </div>
      ) : (
        <div className="w-full h-full flex items-start justify-center">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="w-full max-w-lg  py-2 flex-col flex gap-3"
          >
            <div className="form-control w-full mb-2">
              <label className="label mb-2">
                <span className="label-text">Category Name</span>
              </label>
              <input
                {...register("name", { required: true })}
                type="text"
                placeholder="Type here"
                className="input input-bordered w-full mb-2"
              />
              {errors.name && (
                <span className="text-red-500 text-xs mb-2">This field is required</span>
              )}
            </div>
            <div className="form-control w-full max-w-full py-2">
              <label className="label">
                <span className="label-text mr-6">Choose Parent (optional)</span>
              </label>
              <select
                {...register("parentID", { required: true })}
                className="select select-bordered"
              >
                <option disabled selected value={"-1"}>
                  Pick one category{" "}
                </option>
                {categoryData?.map((item: CategoryData) => {
                  return (
                    <option key={item.id} value={item.id}>
                      {item.title}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="border rounded-md py-1 bg-orange-600 text-white">Done !</button>
          </form>
        </div>
      )}

      <ToastContainer />
    </div>
  );
}
