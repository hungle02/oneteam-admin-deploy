"use Client";

import React, { useEffect, useState } from "react";

import { useSWRConfig } from "swr";
import { toast } from "react-toastify";
import DataTable from "react-data-table-component";
import Image from "next/image";
import Loading from "@/app/loading";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/Store/store";
import { useRouter } from "next/navigation";
import { delete_a_product } from "@/Services/Admin/product";
import { delete_a_bookmark_item, get_all_bookmark_items } from "@/Services/common/bookmark";
import { setBookmark } from "@/utils/Bookmark";
import { update_order_status } from "@/Services/Admin/order";
import { IOrderItem, Order, OrderStatus } from "./OrdersDetailsDataTable";
import { useToast } from '../contexts/ToastContext';
import { setNavActive } from "@/utils/AdminNavSlice";

export default function PendingOrdersDataTable() {
  const { mutate } = useSWRConfig();
  const router = useRouter();
  const [orderData, setOrderData] = useState<Order[] | []>([]);
  const data = useSelector((state: RootState) => state.Admin.Order) as Order[] | [];
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState<Order[] | []>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { toastSuccess, toastError } = useToast();
  const dispatch = useDispatch();

  useEffect(() => {
    const filterPendingOrder = data?.filter((item) => item?.status === OrderStatus.pending);
    setOrderData(filterPendingOrder);
  }, [data]);

  useEffect(() => {
    setFilteredData(orderData);
  }, [orderData]);

  const updateOrderStatus = async (id: string) => {
    setIsLoading(true);
    const res = await update_order_status(id);
    if (res?.id) {
      toastSuccess(res?.message || "Update order status successfully!");
      mutate("gettingAllOrdersForAdmin");
    } else {
      toastError(res?.message || "Update order status failed!");
    }
    setIsLoading(false);
  };

  const columns = [
    {
      name: "Order ID",
      selector: (row: Order) => row?.id,
      sortable: true,
    },
    {
      name: "Total Price",
      selector: (row: Order) =>
        row?.orderItems.reduce(
          (acc, currItem: IOrderItem) =>
            acc +
            ((currItem.productItem.price * (100 - currItem.productItem.discountRate || 0)) / 100) *
              currItem.quantity,
          0
        ),
      sortable: true,
    },
    {
      name: "Delivered",
      selector: (row: Order) => (row?.status === OrderStatus.delivered ? "Yes" : "No"),
      sortable: true,
    },
    {
      name: "Action",
      cell: (row: Order) => (
        <>
          <button
            onClick={() => updateOrderStatus(row?.id)}
            className=" w-20 py-2 mx-2 text-xs text-blue-600 hover:text-white my-2 hover:bg-blue-600 border border-blue-600 rounded transition-all duration-700"
          >
            Delivered
          </button>

          <button
            onClick={() => {
              localStorage.setItem("orderDetailId", JSON.stringify(row?.id));
              dispatch(setNavActive("orderDetail"))
            }}
            className=" w-20 py-2 mx-2 text-xs text-green-600 hover:text-white my-2 hover:bg-green-600 border border-green-600 rounded transition-all duration-700"
          >
            Details
          </button>
        </>
      ),
    },
  ];

  useEffect(() => {
    if (search === "") {
      setFilteredData(orderData);
    } else {
      setFilteredData(
        orderData?.filter((item) => {
          const itemData = item?.id?.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        })
      );
    }
  }, [search, orderData]);

  return (
    <div className="w-full h-full">
      <DataTable
        columns={columns}
        data={filteredData || []}
        key={"ThisOrdersData"}
        pagination
        keyField="id"
        title={`Orders list`}
        fixedHeader
        fixedHeaderScrollHeight="700px"
        selectableRows
        selectableRowsHighlight
        persistTableHead
        subHeader
        subHeaderComponent={
          <input
            className="w-60 dark:bg-transparent py-2 px-2  outline-none  border-b-2 border-orange-600"
            type={"search"}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={"Orders ID"}
          />
        }
        className="bg-white px-4 h-5/6 "
      />
    </div>
  );
}
