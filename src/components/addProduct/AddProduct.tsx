"use client";

import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useForm, SubmitHandler, set } from "react-hook-form";
// import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";
import { ToastContainer, toast } from "react-toastify";
import { TailSpin } from "react-loader-spinner";
import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import { RootState } from "@/Store/store";
import Cookies from "js-cookie";
import useSWR, { useSWRConfig } from "swr";
import { add_new_product } from "@/Services/Admin/product";
import { get_parent_category, get_size } from "@/Services/Admin/category";
import { CategoryData } from "../CategoryDataTable";
import ModalPost from "./Upload";
import { UploadFile } from "antd";

type Inputs = {
  name: string;
  description: string;
  price: Number;
  // quantity: Number;
  categoryID: string;
  // image: Array<File>
  image: File;
  sizeID: string;
  quantity: Number;
  discount: Number;
};

interface Size {
  id: number;
  value: string;
}

export default function AddProduct() {
  const isUploadImage = useSelector((state: RootState) => state.Admin.isUploadImage);
  const [loader, setLoader] = useState(false);
  const [categoryError, setCategoryError] = useState(false);
  const [sizeError, setSizeError] = useState(false);
  const [imageError, setImageError] = useState(false);

  const [fileList, setFileList] = useState<UploadFile[]>([]);

  const Router = useRouter();
  const category = useSelector((state: RootState) => state.Admin.category) as
    | CategoryData[]
    | undefined;
  // const { data: parentCategoryData, isLoading: parentCategoryLoading } = useSWR(
  //   "/getParentCategory",
  //   get_parent_category
  // );
  const { data: sizeData, isLoading: sizeLoading } = useSWR("/getSizes", get_size);
  useEffect(() => {
    const accessToken = localStorage.getItem("accessToken");
    if (!accessToken) {
      Router.push("/");
    }
  }, [Router]);

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm<Inputs>({
    criteriaMode: "all",
  });

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    setLoader(true);
    // const CheckFileSize = maxSize(data.image[0]);
    // if (CheckFileSize) return toast.error("Image size must be less then 1MB");
    // const uploadImageToFirebase = await uploadImages(data.image[0]);
    // const uploadImageToFirebase = 'https://firebasestorage.googleapis.com/v0/b/socialapp-9b83f.appspot.com/o/ecommerce%2Fcategory%2Fimages131.jpg-1683339363348-c4vcab?alt=media&token=f9303ff9-7d34-4514-a53f-832f72814337';
    if (data.categoryID === "Pick one category") {
      setCategoryError(true);
      setLoader(false);
      return;
    }

    if (fileList.length === 0) {
      setImageError(true);
      setLoader(false);
      return;
    }

    if (data.sizeID === "Pick one size") {
      setSizeError(true);
      setLoader(false);
      return;
    }

    const finalData = {
      name: data.name,
      description: data.description,
      productImage: fileList[0].thumbUrl,
      categoryId: data.categoryID,
      price: data.price,
      sizeId: data.sizeID,
      quantity: data.quantity,
      discount: data.discount,
    };
    console.log("Push", finalData);
    const res = await add_new_product(finalData);
    if (res.success) {
      // toast.success(res?.message);
      setTimeout(() => {
        Router.push("/Dashboard");
      }, 2000);
      setLoader(false);
    } else {
      // toast.error(res?.message);
      setLoader(false);
    }
  };

  return (
    <div className="w-full bg-white flex flex-col " style={{ height: "100vh" }}>
      <div className="w-full my-2 text-left">
        <h1 className="text-xl px-4 py-2">Add Product</h1>
      </div>
      {!isUploadImage && (loader || sizeLoading) ? (
        <div className="w-full  flex-col h-96 flex items-center justify-center ">
          <TailSpin
            height="200"
            width="200"
            color="orange"
            ariaLabel="tail-spin-loading"
            radius="1"
            wrapperStyle={{}}
            wrapperClass=""
            visible={true}
          />
          <p className="text-sm mt-2 font-semibold text-orange-500">
            Adding Product Hold Tight ....
          </p>
        </div>
      ) : (
        <div className="w-full h-full flex items-start justify-center">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="w-full max-w-lg  py-2 flex-col flex gap-4"
          >
            <div className="form-control w-full max-w-full py-2">
              <label className="label mr-6">
                <span className="label-text">Choose Category</span>
              </label>
              <select
                {...register("categoryID", { required: true })}
                className="select select-bordered text-gray-600"
              >
                <option disabled selected>
                  Pick one category{" "}
                </option>
                {category?.map((item) => {
                  return (
                    <option key={item.id} value={item.id}>
                      {item.title}
                    </option>
                  );
                })}
              </select>
              {categoryError && (
                <span className="text-red-500 text-xs ml-4">This field is required</span>
              )}
            </div>
            <div className="form-control w-full">
              <label className="label mb-2">
                <span className="label-text">Product Name</span>
              </label>
              <input
                {...register("name", { required: true })}
                type="text"
                placeholder="Type here"
                className="input input-bordered w-full mb-2"
              />
              {errors.name && (
                <span className="text-red-500 text-xs mb-2">This field is required</span>
              )}
            </div>
            <div className="form-control">
              <label className="label mb-2">
                <span className="label-text">Product Description</span>
              </label>
              <textarea
                {...register("description", { required: true })}
                className="textarea textarea-bordered h-16 w-full"
                placeholder="Description"
              ></textarea>
              {errors.description && (
                <span className="text-red-500 text-xs mt-2">This field is required</span>
              )}
            </div>

            <div className="w-full text-xl">
              <p className="mt-4 mb-0">Add Product Variants</p>
            </div>
            <div className="form-control w-full pt-2">
              <label className="label mb-2">
                <span className="label-text">Add product Image</span>
              </label>
              <ModalPost fileList={fileList} setFileList={setFileList} />

              {imageError && (
                <span className="text-red-500 text-xs mb-2">
                  This field is required and the image must be less than or equal to 1MB.
                </span>
              )}
            </div>
            <div className="form-control w-full max-w-full py-2">
              <label className="label mr-6">
                <span className="label-text">Choose Size</span>
              </label>
              <select
                {...register("sizeID", { required: true })}
                className="select select-bordered"
              >
                <option disabled selected>
                  Pick one size{" "}
                </option>
                {sizeData?.map((item: Size) => {
                  return (
                    <option key={item.id} value={item.id}>
                      {item.value}
                    </option>
                  );
                })}
              </select>
              {sizeError && (
                <span className="text-red-500 text-xs ml-4">This field is required.</span>
              )}
            </div>
            <div className="form-control w-full mb-2">
              <label className="label mb-2">
                <span className="label-text">Quantity</span>
              </label>
              <input
                {...register("quantity", { required: true })}
                type="number"
                min={1}
                placeholder="Type here"
                className="input input-bordered w-full mb-2"
              />
              {errors.price && (
                <span className="text-red-500 text-xs mb-2">This field is required</span>
              )}
            </div>
            <div className="form-control w-full mb-2">
              <label className="label mb-2">
                <span className="label-text">Product Price</span>
              </label>
              <input
                {...register("price", { required: true })}
                type="number"
                min={0}
                placeholder="Type here"
                className="input input-bordered w-full mb-2"
              />
              {errors.price && (
                <span className="text-red-500 text-xs mb-2">This field is required</span>
              )}
            </div>
            <div className="form-control w-full mb-2">
              <label className="label mb-2">
                <span className="label-text">DiscountRate</span>
              </label>
              <input
                {...register("discount", { required: true })}
                type="number"
                max={100}
                min={0}
                placeholder="Type here"
                className="input input-bordered w-full mb-2"
              />
              {errors.price && (
                <span className="text-red-500 text-xs mb-2">This field is required</span>
              )}
            </div>
            <button className="border rounded-md py-1 bg-orange-600 text-white">Done !</button>
            <div className="w-full h-20"></div>
          </form>
        </div>
      )}

      <ToastContainer />
    </div>
  );
}
