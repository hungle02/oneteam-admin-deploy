"use client";

import React, { useState } from "react";
import { ConfigProvider, Modal, Upload } from "antd";
import ImgCrop from "antd-img-crop";
import type { RcFile, UploadFile, UploadProps } from "antd/es/upload/interface";

interface Props {
  setFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  fileList: UploadFile[];
}
// fix css component
const ModalPost = ({ fileList, setFileList }: Props) => {
  // const [confirmLoading, setConfirmLoading] = useState(false);
  // const [description, setDescription] = useState("");
  // const [fileList, setFileList] = useState<UploadFile[]>([]);
  const onChange: UploadProps["onChange"] = ({ fileList: newFileList }) => {
    setFileList(newFileList);
    console.log("fileList", newFileList);
  };

  const onPreview = async (file: UploadFile) => {
    let src = file.url as string;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj as RcFile);
        reader.onload = () => resolve(reader.result as string);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  return (
    <div className="flex items-center justify-center">
      <ConfigProvider>
        <ol className="mt-[50px]">
          <ImgCrop rotationSlider>
            <Upload
              action="https://run.mocky.io/v3/0713439f-7371-44db-93f5-1ddd3f2b8630"
              listType="picture-card"
              fileList={fileList}
              onChange={onChange}
              onPreview={onPreview}
              maxCount={1}
            >
              {fileList.length < 5 && "+ Upload"}
            </Upload>
          </ImgCrop>
        </ol>
      </ConfigProvider>
    </div>
  );
};

export default ModalPost;
