"use client"
import React, { useState } from 'react';
// import { useRouter } from 'next/router';
import Image from 'next/image';
import useSWR from 'swr';
import moment from 'moment-timezone';
import "bootstrap/dist/css/bootstrap.css";
import './style.css';
// import { useAuth } from 'contexts/auth';

// import ModalAddProductVariant from 'components/mall/MallProducts/ModalAddProductVariant';
// import ModalEditProductVariant from 'components/mall/MallProducts/ModalEditProductVariant';
// import ModalDeleteProductVariant from 'components/mall/MallProducts/ModalDeleteProductVariant';
// import ModalEditProduct from 'components/mall/MallProducts/ModalEditProduct';
// import ModalDeleteProduct from 'components/mall/MallProducts/ModalDeleteProduct';

import buttonStyles from 'styles/modules/buttons.module.scss';
import styles from 'components/mall/MallProducts/ImageBox.module.css';
import { get_order_detail_by_id } from '@/Services/Admin/product';

export default function ProductDetailPage() {
  const id = localStorage.getItem("orderDetailId");

  const [showAddVariantModal, setShowAddVariantModal] = useState(false);
  const [showEditVariantModal, setShowEditVariantModal] = useState(false);
  const [showDeleteVariantModal, setShowDeleteVariantModal] = useState(false);
  const [actionedVariant, setActionedVariant] = useState(null);

  const [showEditProductModal, setShowEditProductModal] = useState(false);
  const [showDeleteProductModal, setShowDeleteProductModal] = useState(false);

  const { data, isLoading } = useSWR('/gettingOrderByID', () => get_order_detail_by_id(id || ''))

  const tbody = (data?.orderItems ?? []).map((item: any) => {
    return (
      <tr key={item?.productItem?.id}>
        <td style={{ textAlign: 'center' }} >{item?.productItem?.id}</td>
        <td style={{ textAlign: 'center' }} >{item?.productName}</td>
        <td style={{ textAlign: 'center', justifyContent: 'center', alignItems: 'center', display: 'flex' }} ><img alt='product image' src={item?.productItem?.image} style={{ width: 300, height: 300 }} /></td>
        <td style={{ textAlign: 'center' }} >{item?.productItem?.size}</td>
        <td style={{ textAlign: 'center' }} >x{item?.quantity}</td>
        <td style={{ textAlign: 'center' }} >{Number(item?.productItem?.price || 0).toLocaleString()}₫</td>
        <td style={{ textAlign: 'center' }} >{item?.productItem?.discountRate}%</td>
        <td style={{ textAlign: 'center' }} >{item?.productItem?.price * (100 - (item?.productItem?.discountRate || 0)) / 100}</td>
        {/* <td style={{ textAlign: 'center' }}>{moment(item.updatedAt).format('YYYY-MM-DD HH:mm:ss')}</td> */}
      </tr>
    );
  });

  // const productImages = (data?.imagePreviews ?? []).map((imagePreview) => {
  //   return (
  //     <div className="col-4 col-xxl-3">
  //       <Image
  //         src={imagePreview}
  //         width={300}
  //         height={300}
  //         layout="responsive"
  //         objectFit="contain"
  //         className={styles.imageBox}
  //         unoptimized
  //       />
  //     </div>
  //   );
  // });

  return (
    <div style={{ width: '100%', height: '100vh'}}>
      {data && (
        <div id="cardHeader" className="d-flex justify-content-between align-items-center py-1">
          <span style={{ fontSize: '1.2rem', lineHeight: '38px', fontWeight: 'bold' }}>Order #{data?.id}</span>
        </div>
      )}
      <div id="card" style={{ width: '100%' }}>
        {data && (
        <>
          <div className="container-fluid p-0">
            <div className="row">
              <div className="col-10">
                <div className="row py-3">
                  <div className="col-2 border-end">Username</div>
                  <div className="col-10">{data?.users?.username}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Address</div>
                  <div className="col-10">No. 15/4 A, Le Hong Phong Street, Ward 5, My Tho City, Tien Giang province, Vietnam, near Ao Thiet park.</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Status</div>
                  <div className="col-10">{data?.status}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Ordered at:</div>
                  {/* <div className="col-10">{new Date(data?.createdAt).toLocaleDateString()}</div> */}
                  <div className="col-10">{moment(data?.updatedAt).format('DD-MM-YYYY')}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Delivered at:</div>
                  <div className="col-10">{data?.status === 'delivered' ? moment(data?.deliveredAt).format('DD-MM-YYYY') : 'Not yet'}</div>
                </div>
              </div>
            </div>
          </div>
          <br />
          <div>
            <p style={{ fontSize: '1rem', lineHeight: '38px', fontWeight: '600' }}>Order items</p>
            <div>
              {!data?.orderItems?.length && <>None</>}
              {data?.orderItems?.length > 0 && (
                <table >
                  <colgroup>
                    <style jsx>{`
                      colId {
                        width: 12%;
                      }
                      colName {
                        width: 12%;
                      }
                      colImage {
                        width: 15%;
                      }
                      colSize {
                        width: 12%;
                      }
                      colQuantity {
                        width: 12%;
                      }
                      colPrice {
                        width: 12%;
                      }
                      colDiscount {
                        width: 12%;
                      }
                      colCharged {
                        width: 12%;
                      }
                    `}
                    </style>
                    <col className="colId" />
                    <col className="colName" />
                    <col className="colImage" />
                    <col className="colSize" />
                    <col className="colQuantity" />
                    <col className="colPrice" />
                    <col className="colDiscount" />
                    <col className="colCharged" />
                  </colgroup>
                  <thead>
                    <style jsx>{`
                      th {
                        text-align: center;
                      }
                    `}
                    </style>
                    <tr>
                      <th>ID</th>
                      <th>Product name</th>
                      <th>Image</th>
                      <th>Size</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Discount</th>
                      <th>Charged price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {tbody}
                  </tbody>
                </table>
              )}
            </div>
          </div>
          <br />
        </>
        )}
      </div>
    </div>
  );
}
