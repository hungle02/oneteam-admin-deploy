import React, { useEffect, useRef } from "react";
import { Avatar, Flex, Text } from "@chakra-ui/react";
import type { Message } from "../interfaces";
import moment from 'moment-timezone';

interface MessagesProps {
  messages: Message[];
  photo: string;
}

const Messages: React.FC<MessagesProps> = ({ messages, photo }) => {
  const AlwaysScrollToBottom: React.FC = () => {
    const elementRef = useRef<HTMLDivElement>(null);
    useEffect(() => {
      if (elementRef.current) {
        elementRef.current.scrollIntoView();
      }
    });

    return <div ref={elementRef} />;
  };  

  return (
    <Flex w="100%" h="98%" overflowY="scroll" flexDirection="column" p="3">
      {messages.map((item, index) => {
        const currentData = new Date(item?.created_at|| new Date());

        if (item.from_who === "me") {
          return (
            <Flex key={item.id} w="100%" justify="flex-end">
              <Flex bg="blue.100" minW="100px" maxW="450px" my="1" p="3" borderRadius="15px" direction={"column"}>
                <Text>{item.msg}</Text>
                <p style={{ fontSize: "0.8rem", textAlign: "right", marginBottom: "0", marginTop: "-0rem", marginRight: "0.5rem", color: "gray" }}>
                  {moment(currentData).format('HH:MM')}
                </p>
              </Flex>
            </Flex>
          );
        } else {
          return (
            <Flex key={item.id} w="100%">
              <Avatar name="Computer" src={photo} bg="blue.300"></Avatar>
              {/* <Avatar name="Computer" src="./octocat.png" bg="blue.300"></Avatar> */}
              <Flex
                bg="gray.100"
                color="black"
                minW="100px"
                maxW="450px"
                my="1"
                p="3"
                borderRadius="15px"
                direction={"column"}
              >
                <Text>{item.msg}</Text>
                <p style={{ fontSize: "0.8rem", textAlign: "right", marginBottom: "0", marginTop: "-0rem", marginRight: "0.5rem", color: "gray" }}>
                  {moment(currentData).format('HH:MM')}
                </p>
              </Flex>
            </Flex>
          );
        }
      })}
      <AlwaysScrollToBottom />
    </Flex>
  );
};

export default Messages;
