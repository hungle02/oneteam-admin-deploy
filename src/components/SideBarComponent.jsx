/* eslint-disable jsx-a11y/click-events-have-key-events */
"use client";

import React, { useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useRouter, usePathname  } from 'next/navigation';
import { useUser } from "../contexts/UserContext";

export default function SideBarComponent({  }) {
  const [user, setUser] = useUser();
  const onMinimize = (min) => setMinimized(min);
  const router = useRouter();
  const pathname = usePathname();

  const [minimized, setMinimized] = useState(false);

  const buttonLeftPos = minimized ? '10px' : '210px';


  return (
    <>
      <nav className={`sidebar${minimized ? ' minimized' : ''}`}>
        <table className="sidebarUserTable">
          <tbody>
            <tr>
              <td rowSpan="2" className="sidebarImageTD">
                <Image
                    src={'https://scontent.fsgn18-1.fna.fbcdn.net/v/t39.30808-1/371092189_1864740247257487_559782449409085722_n.jpg?stp=dst-jpg_p200x200&_nc_cat=110&ccb=1-7&_nc_sid=5f2048&_nc_eui2=AeG99HPRJXtXQHqOD-vF_dXeHgs7P14QcWIeCzs_XhBxYi48ZmROQ4hLgh7jx5HvxMGIeWA3_omxE5m9a2Qapp9g&_nc_ohc=UXNPrc2zXHgQ7kNvgEkH1Ws&_nc_ht=scontent.fsgn18-1.fna&oh=00_AYD4HScUCM64VQ9DRtWI-5pWnQnjrvO0B0D9-TsfOW1oag&oe=6660A70F'}
                    alt="user profile photo"
                    width={50}
                    height={50}
                    layout="intrinsic"
                    objectFit="cover"
                    unoptimized
                  />
              </td>
              <td>
                <span>{user?.username || 'Lê Quốc Hưng'}</span>
              </td>
            </tr>
            <tr>
              <td>
                <a>
                  <button
                    type="button"
                    onClick={() => {
                      router.push('/authen/login');
                    }}
                  >
                    <span>Log out</span>
                  </button>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        <hr />
        <ul style={{ minHeight: '100vh' }}>
          {/* FAQ */}
          <li className={`${pathname?.endsWith('/faq') ? 'sidebarActive' : ''}`}>
            <a href="/faq" className="sidebarHover">
              <div>
                <span className="sidebarNoselect">FAQ</span>
              </div>
            </a>
          </li>
          {/* Chat */}
          <li className={`${pathname?.endsWith('/chat') ? 'sidebarActive' : ''}`}>
            <a href="/chat" className="sidebarHover">
              <div>
                <span className="sidebarNoselect">Chat</span>
              </div>
            </a>
          </li>
        </ul>
      </nav>
      <button
        type="button"
        style={{
          position: 'fixed',
          top: '10px',
          left: buttonLeftPos,
          padding: '4px',
          display: 'inline',
          border: '1px solid #b0b0b0',
          cursor: 'pointer',
        }}
        onClick={() => {
          setMinimized(!minimized);
          onMinimize(!minimized);
        }}
      >
        <span style={{ margin: 0, color: '#5c5c5c' }}>{minimized ? '>>' : '<<'}</span>
      </button>
    </>
  );
}
