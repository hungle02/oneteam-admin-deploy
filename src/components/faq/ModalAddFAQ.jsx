"use client";

import React, { useEffect, useState } from 'react';

import { api } from '../../interfaces/api';
import { useToast } from '../../contexts/ToastContext';

import Loader from '../Loaders/Loader';

import modalStyles from '../../styles/modules/modal.module.css';
import buttonStyles from '../../styles/modules/buttons.module.scss';

export default function ModalAddFAQCategory({ setShowModal, callback }) {
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState('');
  const { toastSuccess, toastError } = useToast();
  const [formSubmitting, setFormSubmitting] = useState(false);

  function escFunction(event) {
    if (event.keyCode === 27) {
      setShowModal(false);
    }
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('keydown', escFunction);
    }
  }, []);

  function formSubmitHandler(event) {
    event.preventDefault();

    if (question === '') {
      toastError('Question is required');
      return;
    }
    if (answer === '') {
      toastError('Answer is required');
      return;
    }
    setFormSubmitting(true);

    api.post('/chat/admin/faq', { question, answer })
      .then((res) => {
        setFormSubmitting(false);
        toastSuccess(res.data?.message || 'FAQ added successfully');
        callback();
        setShowModal(false);
      })
      .catch((err) => {
        setFormSubmitting(false);
        const msg = err?.response?.data?.message || false;
        toastError(msg || 'Failed to add FAQ');
      });
  }

  return (
    <>
      <div className={modalStyles.modalBox} style={{ left: 'calc(50vw - 325px)', width: '650px' }}>
        <button
          type="button"
          className={modalStyles.modalCloseButton}
          disabled={formSubmitting}
          onClick={() => setShowModal(false)}
        >
          <span className={`material-icons ${modalStyles.modalClose}`}>close</span>
        </button>
        <div className={modalStyles.modalHeader}>Add FAQ</div>
        <form onSubmit={(e) => { formSubmitHandler(e); }}>
          <div className={modalStyles.modalBody}>
            <div className="container-fluid">
              <div className="row mb-3">
                <div className={`col-3 align-items-baseline ${modalStyles.vAlign}`}>
                  <label htmlFor="qTextArea">Question: </label>
                </div>
                <div className={`col-9 align-items-baseline ${modalStyles.vAlign}`}>
                  <textarea
                    name="qTextArea"
                    id="qTextArea"
                    required
                    placeholder="Question"
                    autoComplete="off"
                    defaultValue={question}
                    onChange={({ target }) => {
                      setQuestion(target.value);
                    }}
                  />
                  <span className={modalStyles.required}>*</span>
                </div>
              </div>
              <div className="row">
                <div className={`col-3 align-items-baseline ${modalStyles.vAlign}`}>
                  <label htmlFor="aTextArea">Answer: </label>
                </div>
                <div className={`col-9 align-items-baseline ${modalStyles.vAlign}`}>
                  <textarea
                    name="aTextArea"
                    id="aTextArea"
                    required
                    placeholder="Answer"
                    autoComplete="off"
                    defaultValue={answer}
                    onChange={({ target }) => {
                      setAnswer(target.value);
                    }}
                  />
                  <span className={modalStyles.required}>*</span>
                </div>
              </div>
            </div>
          </div>
          <div className={modalStyles.modalFooter}>
            <button
              disabled={formSubmitting}
              type="submit"
              className={buttonStyles.addButton}
            >
              <span>Create</span>
            </button>
            <button
              disabled={formSubmitting}
              type="button"
              className={buttonStyles.cancelButton}
              onClick={() => setShowModal(false)}
            >
              <span>Cancel</span>
            </button>
            {formSubmitting && (
              <Loader />
            )}
          </div>
        </form>
      </div>
      <div className={modalStyles.blurBox} />
    </>
  );
}
