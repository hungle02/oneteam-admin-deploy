"use client";

import React, { useEffect, useState } from 'react';

import { api } from '../../interfaces/api';
import { useToast } from '../../contexts/ToastContext';

import Loader from '../Loaders/Loader';

import modalStyles from '../../styles/modules/modal.module.css';
import buttonStyles from '../../styles/modules/buttons.module.scss';

// eslint-disable-next-line max-len
export default function ModalDeleteFAQ({
  faq, setShowModal, callback,
}) {
  const [formSubmitting, setFormSubmitting] = useState(false);
  const { toastSuccess, toastError } = useToast();

  function escFunction(event) {
    if (event.keyCode === 27) {
      setShowModal(false);
    }
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('keydown', escFunction);
    }
  }, []);

  function formSubmitHandler(event) {
    event.preventDefault();

    if ((faq.id ?? null) === null) {
      toastError('FAQ ID is required');
      return;
    }
    setFormSubmitting(true);

    api.delete(`/chat/admin/faq/${faq.id}`)
      .then(() => {
        setFormSubmitting(false);
        toastSuccess('FAQ deleted successfully');
        callback();
        setShowModal(false);
      })
      .catch((err) => {
        setFormSubmitting(false);
        const msg = err?.response?.data?.message || false;
        toastError(msg || 'Failed to delete FAQ');
      });
  }

  return (
    <>
      <div className={modalStyles.modalBox}>
        <button
          type="button"
          className={modalStyles.modalCloseButton}
          disabled={formSubmitting}
          onClick={() => setShowModal(false)}
        >
          <span className={`material-icons ${modalStyles.modalClose}`}>close</span>
        </button>
        <div className={modalStyles.modalHeader}>Delete FAQ</div>
        <form onSubmit={(e) => { formSubmitHandler(e); }}>
          <div className={modalStyles.modalBody}>
            <div>Are you sure you want to delete this FAQ?</div>
            <div>This action is non-reversible</div>
          </div>
          <div className={modalStyles.modalFooter}>
            <button
              disabled={formSubmitting}
              type="submit"
              className={buttonStyles.deleteButton}
            >
              <span>Delete</span>
            </button>
            <button
              disabled={formSubmitting}
              type="button"
              className={buttonStyles.cancelButton}
              onClick={() => setShowModal(false)}
            >
              <span>Cancel</span>
            </button>
            {formSubmitting && (
              <Loader />
            )}
          </div>
        </form>
      </div>
      <div className={modalStyles.blurBox} />
    </>
  );
}
