"use client";

import React, { useEffect, useState } from 'react';

import { api } from '../../interfaces/api';
import { useToast } from '../../contexts/ToastContext';

import Loader from '../Loaders/Loader';

import modalStyles from '../../styles/modules/modal.module.css';
import buttonStyles from '../../styles/modules/buttons.module.scss';

export default function ModalEditFAQCategory({ faq, setShowModal, callback }) {
  const [question, setQuestion] = useState(faq.question);
  const [answer, setAnswer] = useState(faq.answer);
  const { toastSuccess, toastError } = useToast();
  const [formSubmitting, setFormSubmitting] = useState(false);

  function escFunction(event) {
    if (event.keyCode === 27) {
      setShowModal(false);
    }
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('keydown', escFunction);
    }
  }, []);

  function formSubmitHandler(event) {
    event.preventDefault();
    setFormSubmitting(true);

    api.put(`/chat/admin/faq/${faq.id}`, { question, answer })
      .then((res) => {
        setFormSubmitting(false);
        toastSuccess(res.data?.message || 'FAQ updated successfully');
        callback();
        setShowModal(false);
      })
      .catch((err) => {
        setFormSubmitting(false);
        const msg = err?.response?.data?.message || false;
        toastError(msg || 'Failed to update FAQ');
      });
  }

  return (
    <>
      <div className={modalStyles.modalBox} style={{ left: 'calc(50vw - 375px)', width: '750px' }}>
        <button
          type="button"
          className={modalStyles.modalCloseButton}
          disabled={formSubmitting}
          onClick={() => setShowModal(false)}
        >
          <span className={`material-icons ${modalStyles.modalClose}`}>close</span>
        </button>
        <div className={modalStyles.modalHeader}>
          Rename FAQ Category
        </div>
        <form onSubmit={(e) => { formSubmitHandler(e); }}>
          <div className={modalStyles.modalBody}>
            <div className="container-fluid">
              <div className="row mb-3">
                <div className={`col-3 ${modalStyles.vAlign}`}>
                  <label htmlFor="faq-question">Question: </label>
                </div>
                <div className={`col-9 ${modalStyles.vAlign}`}>
                  <textarea
                    name="faq-question"
                    required
                    id="faq-question"
                    placeholder="FAQ Question"
                    autoComplete="off"
                    value={question}
                    onFocus={({ target }) => { target.select(); }}
                    onChange={({ target }) => {
                      setQuestion(target.value);
                    }}
                  />
                  <span className={modalStyles.required}>*</span>
                </div>
              </div>
              <div className="row">
                <div className={`col-3 ${modalStyles.vAlign}`}>
                  <label htmlFor="faq-answer">Answer: </label>
                </div>
                <div className={`col-9 ${modalStyles.vAlign}`}>
                  <textarea
                    name="faq-answer"
                    required
                    id="faq-answer"
                    placeholder="FAQ Answer"
                    autoComplete="off"
                    value={answer}
                    onFocus={({ target }) => { target.select(); }}
                    onChange={({ target }) => {
                      setAnswer(target.value);
                    }}
                  />
                  <span className={modalStyles.required}>*</span>
                </div>
              </div>
            </div>
          </div>
          <div className={modalStyles.modalFooter}>
            <button
              disabled={formSubmitting}
              type="submit"
              className={buttonStyles.addButton}
            >
              <span>Update</span>
            </button>
            <button
              disabled={formSubmitting}
              type="button"
              className={buttonStyles.cancelButton}
              onClick={() => setShowModal(false)}
            >
              <span>Cancel</span>
            </button>
            {formSubmitting && (
              <Loader />
            )}
          </div>
        </form>
      </div>
      <div className={modalStyles.blurBox} />
    </>
  );
}
