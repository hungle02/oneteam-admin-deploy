"use Client";

import React, { useEffect, useState } from "react";

import { useSWRConfig } from "swr";
import { toast } from "react-toastify";
import DataTable from "react-data-table-component";
import Image from "next/image";
import Loading from "@/app/loading";
import { RootState } from "@/Store/store";
import { useRouter } from "next/navigation";
import { delete_a_product } from "@/Services/Admin/product";
import { CategoryData } from "./CategoryDataTable";
import { setNavActive } from "@/utils/AdminNavSlice";
import { useDispatch, useSelector } from "react-redux";

export interface ISize {
  id: number;
  value: string;
}
export interface IHasSizes {
  quantity: number;
  sizes: ISize;
}

export interface IProductItemData {
  id: number;
  price: number;
  image: string;
  discountRate: number | 0; //50 => 50%
  hasSizes?: IHasSizes[];
  productId?: number;
}

type ProductData = {
  id: string;
  name: string;
  description: string;
  rating: number;
  // variants: IProductItemData[];
  categories: CategoryData;
};

export default function ProductDataTable() {
  const { mutate } = useSWRConfig();
  const router = useRouter();
  const dispatch = useDispatch();
  const [prodData, setprodData] = useState<ProductData[] | []>([]);
  const data = useSelector((state: RootState) => state.Admin.product);
  const isLoading = useSelector((state: RootState) => state.Admin.productLoading);
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState<ProductData[] | []>([]);

  useEffect(() => {
    setprodData(data);
  }, [data]);

  useEffect(() => {
    setFilteredData(prodData);
  }, [prodData]);

  const columns = [
    {
      name: "Name",
      selector: (row: ProductData) => row?.name,
      sortable: true,
    },
    {
      name: "Category",
      selector: (row: ProductData) => row?.categories?.title,
      sortable: true,
    },
    {
      name: "Action",
      cell: (row: ProductData) => (
        <div className="flex items-center justify-start px-2 h-20">
          <button
            onClick={() => {
              localStorage.setItem("productDetailId", JSON.stringify(row?.id));
              dispatch(setNavActive("productDetail"));
            }}
            className=" w-20 py-2 mx-2 text-xs text-green-600 hover:text-white my-2 hover:bg-green-600 border border-green-600 rounded transition-all duration-700"
          >
            Details
          </button>
          <button
            onClick={() => handleDeleteProduct(row?.id)}
            className=" w-20 py-2 mx-2 text-xs text-red-600 hover:text-white my-2 hover:bg-red-600 border border-red-600 rounded transition-all duration-700"
          >
            Delete
          </button>
        </div>
      ),
    },
  ];

  const handleDeleteProduct = async (id: string) => {
    const res = await delete_a_product(id);
    if (res?.success) {
      toast.success(res?.message);
      mutate("/gettingAllProductsFOrAdmin");
    } else {
      toast.error(res?.message);
    }
  };

  useEffect(() => {
    if (search === "") {
      setFilteredData(prodData);
    } else {
      setFilteredData(
        prodData.filter((item) => {
          const itemData = item.name.toUpperCase();
          const textData = search.toUpperCase();
          return itemData.indexOf(textData) > -1;
        })
      );
    }
  }, [search, prodData]);

  return (
    <div className="w-full h-full">
      <DataTable
        columns={columns}
        data={filteredData || []}
        key={"ThisProductData"}
        pagination
        keyField="id"
        title={`Products list`}
        fixedHeader
        fixedHeaderScrollHeight="500px"
        selectableRows
        selectableRowsHighlight
        persistTableHead
        progressPending={isLoading}
        progressComponent={<Loading />}
        subHeader
        subHeaderComponent={
          <input
            className="w-60 dark:bg-transparent py-2 px-2  outline-none  border-b-2 border-orange-600"
            type={"search"}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder={"Product Name"}
          />
        }
        className="bg-white px-4 h-4/6 "
      />
    </div>
  );
}
