import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "@/Store/store";
import TileContainer from "./TileContainer";
import ProductDataTable from "./ProductDataTable";
import CategoryDataTable from "./CategoryDataTable";
import PendingOrdersDataTable from "./PendingOrdersDataTable";
import CompletedOrderDataTable from "./CompletedOrderDataTable";
import AddProduct from "./addProduct/AddProduct";
import AddCategory from "./addCategory/AddCategory";
import Chat from "../app/chat/page";
import FAQ from "../app/faq/page";
import ProductDetailPage from "./product/ProductDetail";
import OrderDetail from "./order/OrderDetail";

export default function SuperComponent() {
  const navActive = useSelector((state: RootState) => state.AdminNav.ActiveNav);
  switch (navActive) {
    case "Base":
      return <TileContainer />;
    case "activeProducts":
      return <ProductDataTable />;
    case "activeCategories":
      return <CategoryDataTable />;
    case "activePendingOrder":
      return <PendingOrdersDataTable />;
    case "activeDeliveredOrder":
      return <CompletedOrderDataTable />;
    case "activeAddProduct":
      return <AddProduct />;
    case "activeAddCategory":
      return <AddCategory />;
    case "faq":
      return <FAQ />;
    case "chat":
      return <Chat />;
    case "productDetail":
      return <ProductDetailPage />;
    case "orderDetail":
      return <OrderDetail />;
    default:
      return <TileContainer />;
  }
}

export const dynamic = "force-dynamic";
