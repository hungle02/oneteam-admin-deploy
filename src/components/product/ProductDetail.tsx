"use client"
import React, { useState } from 'react';
// import { useRouter } from 'next/router';
import Image from 'next/image';
import useSWR from 'swr';
import moment from 'moment-timezone';
import "bootstrap/dist/css/bootstrap.css";
import './style.css';
// import { useAuth } from 'contexts/auth';

// import ModalAddProductVariant from 'components/mall/MallProducts/ModalAddProductVariant';
// import ModalEditProductVariant from 'components/mall/MallProducts/ModalEditProductVariant';
// import ModalDeleteProductVariant from 'components/mall/MallProducts/ModalDeleteProductVariant';
// import ModalEditProduct from 'components/mall/MallProducts/ModalEditProduct';
// import ModalDeleteProduct from 'components/mall/MallProducts/ModalDeleteProduct';

import buttonStyles from 'styles/modules/buttons.module.scss';
import styles from 'components/mall/MallProducts/ImageBox.module.css';
import { get_product_detail_by_id } from '@/Services/Admin/product';

export default function ProductDetailPage() {
  const id = localStorage.getItem("productDetailId");

  const { data, isLoading } = useSWR('/gettingProductByID', () => get_product_detail_by_id(id || ''))

  const tbody = (data?.variants ?? []).map((item: any) => {
    return (
      <tr key={item?.id}>
        <td style={{ textAlign: 'center' }} >{item?.id}</td>
        <td style={{ textAlign: 'center', justifyContent: 'center', alignItems: 'center', display: 'flex' }} ><img alt='product image' src={item?.image} style={{ width: 300, height: 300 }} /></td>
        <td style={{ textAlign: 'center' }} >
          {
            item?.hasSizes.map((size: any) => {
              return <p key={size?.sizes?.id} >{`x${size.quantity} ${size?.sizes?.value}`}</p>
            })
          }
        </td>
        <td style={{ textAlign: 'center' }} >{item?.price.toLocaleString()}₫</td>
        <td style={{ textAlign: 'center' }} >{item?.discountRate}%</td>
      </tr>
    );
  });

  // const productImages = (data?.imagePreviews ?? []).map((imagePreview) => {
  //   return (
  //     <div className="col-4 col-xxl-3">
  //       <Image
  //         src={imagePreview}
  //         width={300}
  //         height={300}
  //         layout="responsive"
  //         objectFit="contain"
  //         className={styles.imageBox}
  //         unoptimized
  //       />
  //     </div>
  //   );
  // });

  return (
    <div style={{ width: '100%', height: '100vh'}}>
      {data && (
        <div id="cardHeader" className="d-flex justify-content-between align-items-center py-1">
          <span style={{ fontSize: '1.2rem', lineHeight: '38px', fontWeight: 'bold' }}>Product #{data?.id}</span>
        </div>
      )}
      <div id="card" style={{ width: '100%' }}>
        {data && (
        <>
          <div className="container-fluid p-0">
            <div className="row">
              <div className="col-10">
                <div className="row py-3">
                  <div className="col-2 border-end">ID</div>
                  <div className="col-10">{data?.id}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Name</div>
                  <div className="col-10">{data?.name}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Description</div>
                  <div className="col-10">{data?.description}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Rating</div>
                  <div className="col-10">{data?.rating ?
                  '⭐'.repeat(Math.floor(data?.rating)) :
                  '⭐'
                }</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Category</div>
                  <div className="col-10">{data?.categories?.title}</div>
                </div>
                <div className="row py-3">
                  <div className="col-2 border-end">Created at:</div>
                  {/* <div className="col-10">{new Date(data?.createdAt).toLocaleDateString()}</div> */}
                  <div className="col-10">{moment(data?.created_at).format('DD-MM-YYYY')}</div>
                </div>
              </div>
            </div>
          </div>
          <br />
          <div>
            <p style={{ fontSize: '1rem', lineHeight: '38px', fontWeight: '600' }}>Variants</p>
            <div>
              {!data?.variants?.length && <>None</>}
              {data?.variants?.length > 0 && (
                <table >
                  <colgroup>
                    <style jsx>{`
                      colId {
                        width: 12%;
                      }
                      colImage {
                        width: 15%;
                      }
                      colQuantity {
                        width: 12%;
                      }
                      colPrice {
                        width: 12%;
                      }
                      colDiscount {
                        width: 12%;
                      }
                    `}
                    </style>
                    <col className="colId" />
                    <col className="colImage" />
                    <col className="colQuantity" />
                    <col className="colPrice" />
                    <col className="colDiscount" />
                  </colgroup>
                  <thead>
                    <style jsx>{`
                      th {
                        text-align: center;
                      }
                    `}
                    </style>
                    <tr>
                      <th>ID</th>
                      <th>Image</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Discount Rate</th>
                    </tr>
                  </thead>
                  <tbody>
                    {tbody}
                  </tbody>
                </table>
              )}
            </div>
          </div>
          <br />
        </>
        )}
      </div>
    </div>
  );
}
