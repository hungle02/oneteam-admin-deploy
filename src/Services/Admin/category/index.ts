import Cookies from "js-cookie";
import { Config } from "@/config";

export const get_all_categories = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/categories/`, {
      method: "GET",
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting all Categories (service) =>", error);
  }
};

export const get_parent_category = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/categories/parent`, {
      method: "GET",
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting all Categories (service) =>", error);
  }
};

export const get_size = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/size`, {
      method: "GET",
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting all Categories (service) =>", error);
  }
};

export const add_new_category = async (formData: any) => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/categories`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
      body: JSON.stringify(formData),
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in Add New Category (service) =>", error);
  }
};

export const get_category_by_id = async (id: string) => {
  try {
    const res = await fetch(`/api/common/category/get-category-by-id?id=${id}`, {
      method: "GET",
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting Categories by ID (service) =>", error);
  }
};

export const delete_a_category = async (id: string) => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/categories?id=${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in deleting Categories (service) =>", error);
  }
};

export const update_a_category = async (formData: any) => {
  try {
    const res = await fetch(`/api/Admin/category/update-category`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in updating Categories (service) =>", error);
  }
};
