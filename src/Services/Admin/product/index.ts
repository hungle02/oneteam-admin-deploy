import { Config } from "@/config";
import Cookies from "js-cookie";

export const add_new_product = async (formData: any) => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
      body: JSON.stringify(formData),
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in Add New Category (service) =>", error);
  }
};

export const get_order_number = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}order/orders/count`, {
      method: "GET",
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting product count (service) =>", error);
  }
};

export const get_product_number = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/products/count`, {
      method: "GET",
    });
    const data = await res.json();
    return { products: data };
  } catch (error) {
    console.log("Error in getting product count (service) =>", error);
  }
};

export const get_all_products = async (categoryId: string = "") => {
  try {
    console.log("product result");

    const res = await fetch(`${Config.BACKEND_URL}inventory/admin/products?cursor=0`, {
      method: "GET",
    });
    const data = await res.json();

    console.log("product result", data);

    return data;
  } catch (error) {
    console.log("Error in getting all products (service) =>", error);
  }
};

export const get_user_number = async () => {
  try {
    console.log("get user");

    const res = await fetch(`${Config.BACKEND_URL}auth/users/count`, {
      method: "GET",
    });

    console.log("user count", res);
    const data = await res.json();

    return data;
  } catch (error) {
    console.log("Error in getting user count (service) =>", error);
  }
};

export const get_order_detail_by_id = async (id: string) => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}order/admin/orders/${id}`, {
      method: "GET",
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting Order by ID (service) =>", error);
  }
};

export const get_product_detail_by_id = async (id: string) => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/products/${id}`, {
      method: "GET",
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting Product by ID (service) =>", error);
  }
};

export const get_feature_products = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}inventory/products/trending`, {
      method: "GET",
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting all products (service) =>", error);
  }
};

export const delete_a_product = async (id: string) => {
  try {
    console.log("trigger delete product", id);
    const res = await fetch(`${Config.BACKEND_URL}inventory/products/?id=${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });

    const data = await res.json();
    console.log("delete result ", data);
    return data;
  } catch (error) {
    console.log("Error in deleting Product (service) =>", error);
  }
};

export const update_a_product = async (formData: any) => {
  try {
    const res = await fetch(`/api/Admin/product/update-product`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in updating Product (service) =>", error);
  }
};

export const get_product_by_id = async (id: string) => {
  try {
    const res = await fetch(`/api/common/product/get-product-by-id?id=${id}`, {
      method: "GET",
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting product by ID (service) =>", error);
  }
};

export const get_product_by_category_id = async (id: string) => {
  try {
    const res = await fetch(`/api/common/product/get-product-by-category-id?id=${id}`, {
      method: "GET",
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting product by category ID (service) =>", error);
  }
};
