import { Config } from "@/config";
import Cookies from "js-cookie";

export const get_all_orders = async () => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}order/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });
    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in getting all orders (service) =>", error);
  }
};

export const update_order_status = async (id: any) => {
  try {
    const res = await fetch(`${Config.BACKEND_URL}order/admin/orders/${id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({status: "delivered"}),
    });

    const data = await res.json();
    return data;
  } catch (error) {
    console.log("Error in updating order status (service) =>", error);
  }
};
