export const Config = {
  // API_URL: "https://jsonplaceholder.typicode.com/ ",
  // API_URL: "http://192.168.1.8:8000/api/v1/",
  // WEB_SOCKET_URL: "ws://192.168.1.7:8000/",
  // BACKEND_URL: "http://localhost:8000/api/v1/",
  WEB_SOCKET_URL: "wss://oneteam.onecrew.store/",
  BACKEND_URL: "https://oneteam.onecrew.store/api/v1/",
};
