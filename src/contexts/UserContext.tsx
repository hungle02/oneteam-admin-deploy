'use client';

import React from 'react';

import { UserModel } from '../interfaces/user';

const UserContext = React.createContext<
  [UserModel | null, React.Dispatch<React.SetStateAction<UserModel | null>>] | undefined
>(undefined);

export function UserProvider({ children }: { children: React.ReactNode }) {
  const [user, setUser] = React.useState<UserModel | null>(null);
  return (
    <UserContext.Provider value={[user, setUser]}>
      {children}
    </UserContext.Provider>
  );
}

export function useUser() {
  const context = React.useContext(UserContext);
  if (context === undefined) {
    throw new Error('useUser must be used within a UserProvider');
  }
  return context;
}