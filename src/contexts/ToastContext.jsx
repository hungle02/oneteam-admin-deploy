'use client';
import { createContext, useContext, useState } from 'react';

export const ToastContext = createContext();

const ALERT = {
  SUCCESS: 'toastMessageSuccess',
  INFO: 'toastMessageInfo',
  ERROR: 'toastMessageError',
};

export const ToastProvider = ({ children }) => {
  const [messages, setMessages] = useState([]);

  function hideMessage(id) {
    document.getElementById(id).style.marginTop = '-43px';
    setTimeout((id2) => {
      document.getElementById(id2).style.display = 'none';
    }, 970, id);
  }

  function addMessage(newMessage) {
    setMessages((prevState) => {
      const clone = [...prevState];
      clone.push(newMessage);
      return clone;
    });
  }

  async function toastSuccess(message) {
    addMessage([ALERT.SUCCESS, message]);
  }

  async function toastInfo(message) {
    addMessage([ALERT.INFO, message]);
  }

  async function toastError(message) {
    addMessage([ALERT.ERROR, message]);
  }

  let indexCounter = -1;
  const messagesDiv = messages.map((message) => {
    indexCounter += 1;
    setTimeout(hideMessage, 5 * 1000, `toastAlert${indexCounter}`);
    return (
      // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions,jsx-a11y/click-events-have-key-events
      <div
        key={`${indexCounter}`}
        role="alert"
        id={`toastAlert${indexCounter}`}
        className={`toastMessage ${message[0]}`}
        onClick={(e) => {
          e.target.style.display = 'none';
        }}
      >
        {message[1]}
      </div>
    );
  });

  // TODO: Clear toasts at some point in time
  return (
    <>
      <div className="myToast">{messagesDiv}</div>
      <ToastContext.Provider value={{ toastSuccess, toastInfo, toastError }}>
        {children}
      </ToastContext.Provider>
    </>
  );
};

export const useToast = () => useContext(ToastContext);
