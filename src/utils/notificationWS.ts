/* eslint-disable react-hooks/rules-of-hooks */
"use client"

import React, { useEffect, useRef } from "react";
import { Config } from "../config";
import { useUser } from "../contexts/UserContext";
import { UserModel } from "@/interfaces/user";
import { Message } from "@/interfaces";
// import { Config } from "@/Config";
// import { initChatSocket, removeChatSocket, updateChatSocketStatus } from "@/Store/reducers";
// import { useAppDispatch, useAppSelector } from "@/Hooks";
// import { GiftedChat, IMessage } from "react-native-gifted-chat";
// import { createMessage } from "./chat";
// import { IMessageResponse } from "@/Services/chat";

interface IWebSocket {
  current: WebSocket | null;
}

const notificationWS = () => {
  const ws: IWebSocket =  useRef(null);
  const [user, setUser] = useUser();
  // const dispatch = useAppDispatch();
  // const user = useAppSelector((state) => state.user.user);

  const connectWS = () => {
    console.log('Start connect socket!!')
    const urlWS = `${Config.WEB_SOCKET_URL}api/v1/chat/websocket/-1`;
    ws.current = new WebSocket(urlWS);

    console.log(ws.current)

    ws.current.onopen = () => {
      if (ws.current?.readyState === WebSocket.OPEN) {
        // console.log("Connected to ws server");
        setUser({
          ...user,
          chat: {
            ws_status: "open",
            ws: ws.current,
          }
        } as UserModel)
        // dispatch(
        //   initChatSocket({
        //     ws_status: "open",
        //     ws: ws.current,
        //   })
        // );
        ws.current.send(
          JSON.stringify({
            userId: -1,
            message: "Admin say hello server",
          })
        );
      }
    };

    ws.current.onclose = () => {
      if (user && user?.chat?.ws_status !== "terminate") {
        console.log("Close ws connection");
        console.log("Try to reconnect");
        connectWS();
      } else {
        if (user) {
          // dispatch(removeChatSocket());
          setUser({
            ...user,
            chat: undefined,
          } as UserModel)
        }
      }
    };
    ws.current.onerror = (err) => {
      // setUser({
      //   ...currentUser,
      //   notification: {
      //     unread_count: 0,
      //     ws_status: 'terminate',
      //     ws: ws.current,
      //   },
      // } as UserModel);
      ws.current?.close();
      console.log("Get some error");
    };
  };

  const closeWS = () => {
    if (!ws.current) return;
    if (ws.current?.readyState !== WebSocket.CLOSED) {
      ws.current.close();
    }
  };

  const broadcast = (message: string) => {
    if (!ws.current) return;
    if (ws.current.readyState === WebSocket.OPEN) {
      ws.current.send(message);
    }
  };

  const updateMessage = (targetChatRoomId: string, addMessage: (message: Message) => void) => {
    if (!ws.current) return;
    ws.current.onmessage = (e) => {
      if (ws.current?.readyState === WebSocket.OPEN) {
        console.log("Received data from server: ", e.data);
        if(JSON.parse(e.data).channelId === targetChatRoomId){
          const messageReponse: Message = {
            chat_id: JSON.parse(e.data).id,
            id: JSON.parse(e.data).id,
            created_at: new Date(),
            msg: JSON.parse(e.data).message,
            from_who: JSON.parse(e.data).isAdmin ? "me" : "computer",
          }
          addMessage(messageReponse);
        }
      }
    };
  };

  return { connectWS, broadcast, updateMessage };
};

export default notificationWS;
