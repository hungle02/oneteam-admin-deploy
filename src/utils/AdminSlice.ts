import { createSlice } from "@reduxjs/toolkit";

interface NavState {
  isUploadImage: boolean;
  userNumber: number;
  category: any[];
  catLoading: boolean;
  productLoading: boolean;
  product: any[];
  Order: any[];
  orderLoading: boolean;
}

const initialState: NavState = {
  isUploadImage: false,
  userNumber: 0,
  category: [],
  catLoading: false,
  productLoading: false,
  product: [],
  Order: [],
  orderLoading: false,
};

export const Admin = createSlice({
  name: "AdminData",
  initialState,
  reducers: {
    setIsUploadImage: (state, action) => {
      state.isUploadImage = action.payload;
    },
    setUserNumber: (state, action) => {
      state.userNumber = action.payload;
    },
    setCategoryData: (state, action) => {
      console.log("set categories", action.payload);
      state.category = action.payload;
    },
    setProductData: (state, action) => {
      state.product = action.payload;
    },
    setCatLoading: (state, action) => {
      state.catLoading = action.payload;
    },
    setProdLoading: (state, action) => {
      state.productLoading = action.payload;
    },
    setOrderData: (state, action) => {
      console.log("set order", action.payload);
      state.Order = action.payload;
    },
    setOrderLoading: (state, action) => {
      state.orderLoading = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setIsUploadImage,
  setUserNumber,
  setCategoryData,
  setCatLoading,
  setProdLoading,
  setProductData,
  setOrderData,
  setOrderLoading,
} = Admin.actions;

export const AdminReducer = Admin.reducer;
